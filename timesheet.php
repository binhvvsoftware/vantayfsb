<?php
error_reporting(E_ALL & ~E_NOTICE);
/**
 * Simple script to get data from employee tracker
 * binhvvsoftware@gmail.com
 */

set_time_limit(0);
date_default_timezone_set('Asia/Bangkok');
require __DIR__ . '/conn.php';

// User defined classes
require __DIR__ . '/classes/User.php';
require __DIR__ . '/classes/Timesheet.php';
require __DIR__ . '/classes/TimesheetRaw.php';
require __DIR__ . '/classes/ScanLog.php';

// TAD
require __DIR__ . '/lib/TADFactory.php';
require __DIR__ . '/lib/TAD.php';
require __DIR__ . '/lib/TADResponse.php';
require __DIR__ . '/lib/Providers/TADSoap.php';
require __DIR__ . '/lib/Providers/TADZKLib.php';
require __DIR__ . '/lib/Exceptions/ConnectionError.php';
require __DIR__ . '/lib/Exceptions/FilterArgumentError.php';
require __DIR__ . '/lib/Exceptions/UnrecognizedArgument.php';
require __DIR__ . '/lib/Exceptions/UnrecognizedCommand.php';

use TADPHP\TADFactory;
use TADPHP\TAD;
$excludeUsers = ['Array'];
$aryIP = ['118.69.183.236', '118.69.183.236'];

function reArrangeTime($a, $b) {
    return strcmp($a['DateTime'], $b['DateTime']);
}

function getLogByUid($logs, $userId) {
    if (!$logs) return null;
    return array_filter($logs, function($item) use ($userId) {
        return $item['PIN'] == $userId;
    });
}

try {
    $aryDataLog = [];
    foreach ($aryIP as $IpServer) {
        $optionsPam = [
            'ip' => $IpServer,
            'description' => 'TAD1', // 'N/A' by default.
            'udp_port' => '4370',// 4370 by default.
            'encoding' => 'utf-8'    // iso8859-1 by default.
        ];

        $tadFactory = new TADFactory($optionsPam);
        $tadPyData = $tadFactory->get_instance();

        $userObj = new User($sqlite);

        $aryDataLog = array_merge($aryDataLog, $tadPyData->get_att_log()->to_array()['Row']);
        $users = $tadPyData->get_all_user_info()->to_array();

        echo $userObj->sync($users['Row']) . ' users synced';
    }

    $logObj = new ScanLog($aryDataLog);

	$startDate = isset($_GET['start-date']) ? $_GET['start-date'] : date('Y-m-01');
	$endDate = isset($_GET['end-date']) ? $_GET['end-date'] : date('Y-m-d');

	$scanLogs = $logObj->getScanLog(null, $startDate, $endDate);
	$users = $userObj->fetchAll();

	$userLog = [];
	for($i = 1; $i <= intval(date("t")); $i++) {
		foreach ($users as $key => $user) {
			if (in_array($user['name'], $excludeUsers)) continue;
			$userLog = getLogByUid($scanLogs[date('Y-m-' . sprintf("%02d", $i))], $user['pin2']);
			if (!$userLog) continue;
			usort($userLog, 'reArrangeTime');
			$checkInTime = isset(array_values($userLog)[0]) ? array_values($userLog)[0]['DateTime'] : null;
			$checkoutTime = end($userLog)['DateTime'];
			if (!$checkInTime) continue;
			$timesheet = new Timesheet($sqlite);
			$timesheetRaw = new TimesheetRaw($sqlite);
			$timesheet->insert([
				'emp_code' => $user['pin2'],
				'checkin_time' => $checkInTime,
				'checkout_time' => $checkoutTime,
			]);
			// Insert raw data
			foreach ($userLog as $log) {
				$timesheetRaw->insert([
					'emp_code' => $user['pin2'],
					'checkin_time' => $log['DateTime']
				]);
			}
			echo $user['pin2'] . ' -> ' . $checkInTime . ' - ' .$checkoutTime . '<br>';
		}
	}
}
catch(\Exception $e) {
	echo $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine();die;
}
