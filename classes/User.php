<?php
/**
 * User class
 */
class User {

	private $db = null;

	function __construct($sqlite) {
		$this->db = $sqlite;
	}

	function checkUserExit($id) {
		try {
            $tsql= "SELECT * FROM chamcong.dbo.users WHERE pin1 = ?";
            $params = array($id);
            $getResults= sqlsrv_query($this->db, $tsql, $params);

            if ($getResults == FALSE)
                return false;

            if (sqlsrv_rows_affected($getResults) == 0) {
                return true;
            }

            return false;
            sqlsrv_free_stmt($getResults);
		}
		catch( \Exception $e) {
			echo $e->getMessage();
		}
		return false;
	}

	function fetchAll() {
		try {

            $data = [];
            $tsql= "SELECT * FROM chamcong.dbo.users ORDER BY pin2 ASC;";
            $getResults= sqlsrv_query($this->db, $tsql);
            if ($getResults == FALSE)
                die(FormatErrors(sqlsrv_errors()));

            while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
                array_push($data, $row);
            }

            return $data;
		}
		catch( \Exception $e) {
			echo $e->getMessage();
		}
		return [];
	}

	function sync($users) {
		$syncedUser = 0;
		try {

	        foreach ($users as $user) {
                $pin1 = !empty($user['PIN']) ? $user['PIN'] : 0;
                $pin2 = !empty($user['PIN2']) ? $user['PIN2'] : 0;
                $group = !empty($user['Group']) ? $user['Group'] : '';
                $name =  !empty($user['Name']) ? $user['Name'] : '';
                $privilege = !empty($user['Privilege']) ? $user['Privilege'] : '';
                $card = !empty($user['Card']) ? $user['Card'] : '';
                $raw_data = json_encode($user);

                if ($this->checkUserExit($pin1)) {
                    $insert = "INSERT INTO chamcong.dbo.users (pin1, pin2, name, groupName, privilege, card, raw_data) 
	    		                VALUES (?, ?, ?, ?, ?, ?, ?)";
                    $params = array($pin1, $pin2, $name, $group , $privilege, $card, $raw_data );
                    $getResults= sqlsrv_query($this->db, $insert, $params);
                    $rowsAffected = sqlsrv_rows_affected($getResults);
                    if ($getResults == FALSE or $rowsAffected == FALSE)
                        die(FormatErrors(sqlsrv_errors()));
                    sqlsrv_free_stmt($getResults);
                    $syncedUser++;
                }
	        }
	    }
	    catch( \Exception $e) {
	    	echo $e->getMessage();die;
	    }
        return $syncedUser;
	}
}
