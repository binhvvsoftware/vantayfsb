<?php
/**
 * User class
 */
class ScanLog {
	var $logs = null;
	function __construct($logs) {
		$this->logs = $logs;
	}
	public static function checkDateInRange($start_date, $end_date, $date_from_user) {
	  // Convert to timestamp
	  $start_ts = strtotime($start_date);
	  $end_ts = strtotime($end_date . ' 23:59:59');
	  $user_ts = strtotime($date_from_user);

	  // Check that user date is between start & end
	  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
	}
	
	public function getScanLog($uid = null, $logFromDate = null, $logToDate = null) {
		if( $logFromDate === null) $logFromDate = date('Y-m-01');
		if( $logToDate === null) $logToDate = date('Y-m-d');
		$data = [];
		$attLogs = [];
		try {
			foreach( $this->logs as $log) {
				// Filter by user pin
				if( $uid !== null && $uid != $log['PIN']) continue;
				
				if( self::checkDateInRange($logFromDate, $logToDate, $log['DateTime'])) {
					$data[date('Y-m-d', strtotime($log['DateTime']))][] = $log;
				}
			}
		}
		catch( \Exception $e) {
			echo $e->getMessage();
		}

		return $data;
	}
}