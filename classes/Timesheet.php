<?php
/**
 * User class
 */
class Timesheet {

	private $db = null;

	function __construct($sqlite) {
		$this->db = $sqlite;
	}

    function checkTimesheetExit($timesheet) {
        try {
            $tsql= "SELECT * FROM chamcong.dbo.timesheets WHERE emp_code = ? and checkin_time = ? and checkout_time = ?";
            $params = array($timesheet['emp_code'], $timesheet['checkin_time'], $timesheet['checkout_time']);
            $getResults= sqlsrv_query($this->db, $tsql, $params);

            if ($getResults == FALSE)
                return false;

            if (sqlsrv_rows_affected($getResults) == 0) {
                return true;
            }

            return false;
            sqlsrv_free_stmt($getResults);
        }
        catch( \Exception $e) {
            echo $e->getMessage();
        }
        return false;
    }

	function insert($timesheet) {
		try {
            if ($this->checkTimesheetExit($timesheet)) {
                $insert = "INSERT INTO chamcong.dbo.timesheets (emp_code, checkin_time, checkout_time)
	    		                VALUES (?, ?, ?)";
                $params = array($timesheet['emp_code'], $timesheet['checkin_time'], $timesheet['checkout_time']);
                $getResults= sqlsrv_query($this->db, $insert, $params);
                $rowsAffected = sqlsrv_rows_affected($getResults);
                if ($getResults == FALSE or $rowsAffected == FALSE)
                    die(FormatErrors(sqlsrv_errors()));

                sqlsrv_free_stmt($getResults);

                return true;
            }
	    }
	    catch( \Exception $e) {
	    	echo $e->getMessage();die;
	    }
        return false;
	}
}
