<?php
try {

    $serverName = "localhost";
    $connectionOptions = array(
        "database" => "master",
        "uid" => "sa",
        "pwd" => "myPassw0rd",
        "TraceOn" => true,
        "TraceFile" => "sqlsrvtrace.log",
        "CharacterSet" => "UTF-8",
    );

    // Establishes the connection
    $sqlite = sqlsrv_connect($serverName, $connectionOptions);
    if ($sqlite === false) {
        die(formatErrors(sqlsrv_errors()));
    }

    function formatErrors($errors)
    {
        echo "Error information: <br/>";
        foreach ($errors as $error) {
            echo "SQLSTATE: ". $error['SQLSTATE'] . "<br/>";
            echo "Code: ". $error['code'] . "<br/>";
            echo "Message: ". $error['message'] . "<br/>";
        }
    }
}
catch(\Exception $e) {
	echo $e->getMessage();die;
}
